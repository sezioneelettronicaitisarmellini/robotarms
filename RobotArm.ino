#include <VarSpeedServo.h>
//define pin 
#define basePin 11
#define shoulderPin 10
#define armPin 9
#define wristPin 6
#define clampPin 5
#define P1Pin 8
#define P2Pin 7
#define L1Pin 2
#define L2Pin 4
#define p1 8
#define p2 7

//define index
#define baseI 0
#define shoulderI 1
#define armI 2
#define wristI 3
#define clampI 4

bool seq = false;
bool last = false;

VarSpeedServo armServos[5];
int servoPins[5] = {basePin, shoulderPin, armPin, wristPin, clampPin};
int servoSpeeds[5] = {23, 23, 100, 0, 0};
int movimento[][5] = {
  {30, 100, 60, 63, 75},
  {0, 100, 60, 63, 75},
  {0, 100, 60, 63, 0},
  {0, 50, 130, 63, 0},
  {0, 50, 130, 63, 75},
  {0, 100, 100, 63, 75},
  {0, 100, 100, 63, 75},
  {90, 100, 100, 63, 75},
  {90, 100, 100, 63, 75},
  {90, 50, 130, 63, 75},
  {90, 50, 130, 63, 0},
  {90, 100, 60, 63, 0},
  {90, 100, 60, 63, 75}
};

void setPosition(int positions) {
  int i;
  for (i = 0; i < 5; i++) {
    armServos[i].write(movimento[positions][i], servoSpeeds[i], true);
  }
}
void rotationCW() {
  int i;
  for (i = 1; i < (sizeof(movimento) / sizeof(movimento[0])); i++) {
    setPosition(i);
    delay(50);
  }
  setPosition(0);
}
void rotationCCW() {
  int i;
  for (i = (sizeof(movimento) / sizeof(movimento[0])) - 1; i > 0; i--) {
    setPosition(i);
    delay(50);

  }
  setPosition(0);
}

void stoploop(){
  seq = false;
  Serial.println("*Dinterrupt*");
}

void setup() {
//  pinMode(L1Pin, OUTPUT);
  pinMode(L2Pin, OUTPUT);
  digitalWrite(L1Pin, HIGH);
  digitalWrite(L2Pin, HIGH);
  for (int servo = 0; servo < 5; servo++) {
    armServos[servo].attach(servoPins[servo]);
  }
  pinMode(3, INPUT_PULLUP);
  pinMode(p2, INPUT_PULLUP);
  pinMode(p1, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(3), stoploop, CHANGE);

  armServos[baseI].write(30, 30, true);
  armServos[shoulderI].write(100, 30, true);
  armServos[wristI].write(63, 30, true);
  armServos[clampI].write(0, 30, true);
  Serial.begin(9600);
  Serial.println("*D[+] Start position reached.*");
}

void loop() {
  bool settingSpeed = false;
  bool settingPosition = false;
  String serialString = "";
  if (digitalRead(p1) == 0) {
    seq=true;
    //rotationCW();
  }
//  if (digitalRead(p2) == 0) {
    
    //rotationCCW();
//  }
  if (Serial.available() > 0) {
    serialString = Serial.readStringUntil(';');
  }
  if (serialString != "") {
    switch (serialString[0]) {
      case 'M':
        {
          if (serialString[1] == '0') {
            armServos[clampI].write(0);
            Serial.println("*D[+] Pinza Aperta*");
            Serial.println("*LR255G0B0*");
          }
          else
          {
            armServos[clampI].write(75);
            Serial.println("*D[+] Pinza Chiusa*");
            Serial.println("*LR0G255B0*");
          }
          break;
        }
      case 'A':
        {
          if ((int)serialString[1] - 48) {
            seq = true;
            Serial.println("*D[+] Auto Mode On*");
          }
          else {
            seq = false;
            Serial.println("*D[-] Auto Mode Off*");
          }
        }
      case 'C':
        {
          Serial.println("*D[+] Rotazione Oraria*");
          rotationCW();
          break;
        }
      case 'G':
        {
          Serial.println("*D[+] Rotazione AntiOraria*");
          rotationCCW();
          break;
        }
      case 'S':
        {
          settingSpeed = true;
          Serial.println("*D[+] Setting Speed*");
          break;
        }
      case 'P':
        {
          settingPosition = true;
          Serial.println("*D[+] Setting Position*");
          break;
        }
      default:
        {
          break;
        }
    }
    int motorIndex = int(serialString[1]) - 48;
    int setValue = serialString.substring(2).toInt();
    if (settingSpeed) {
      Serial.print("*D[+] Motor Index ");
      Serial.print(motorIndex);
      Serial.print(" \a ");
      Serial.print(setValue);
      Serial.println("*");
      servoSpeeds[motorIndex] = setValue;
    }
    else if (settingPosition) {
      Serial.print("*D[+] Motor Index ");
      Serial.print(motorIndex);
      Serial.print(" a ");
      Serial.print(setValue);
      Serial.println("*");
      armServos[motorIndex].write(setValue, servoSpeeds[motorIndex], true);
    }
    Serial.println(serialString);
  }
  Serial.flush();
  if (seq) {
    if (last)
      rotationCW();
    else
      rotationCCW();
    last = !last;
  }
}
